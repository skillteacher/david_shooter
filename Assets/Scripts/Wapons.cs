using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Wapons : MonoBehaviour
{
    [SerializeField] private Transform cameraTransfrom;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private GameObject spaksPrefab;
    [SerializeField] private float sparksLifetime = 0.1f;
    


    private void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            PlayEffect();

            Shoot();
        }

    }

    private void Shoot()
    {

        RaycastHit hit;
        if (!Physics.Raycast(cameraTransfrom.position, cameraTransfrom.forward, out hit, range)) return;
        HitEffect(hit.point);
        Health health = hit.transform.GetComponent<Health>();
        if (health == null) return;
        health.TakeDamage(damage);

    }
    private void PlayEffect()
    {
        muzzleFlash.Play();
    }
    private void HitEffect(Vector3 point )
    {
        GameObject sparks;
        sparks = Instantiate(spaksPrefab,point,Quaternion.identity);
        Destroy(sparks, sparksLifetime);
       
    }
}

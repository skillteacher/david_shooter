using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(EnemyData))]
public class EnemyMovement : MonoBehaviour
{
    
    [SerializeField] private float visionRange = 10f;
    private EnemyData enemyData;
        
    
    
    private void Awake()
    {
      enemyData = GetComponent<EnemyData>();  
    }

    private void Update()
    {
        if (IsTargetInVisionRange())

        {
            if (enemyData.distanceToTarget > enemyData.agent.stoppingDistance)
            {
                enemyData.animator.SetBool("run", true);
            }
            else
            {
                enemyData.animator.SetBool("run", false);
            }

            enemyData. agent.SetDestination(enemyData.target.position);
        }
    }

    private bool IsTargetInVisionRange()
    {
            return visionRange > enemyData.distanceToTarget;
    }


    private void OnDrawGizmos()

    {
        Gizmos.DrawWireSphere(transform.position, visionRange);
    }
}

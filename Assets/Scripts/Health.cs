using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float startHitPoints = 100f;
    [SerializeField] private bool isPlayer;
    private float hitPoints;
    private void Start()
    {
        hitPoints = startHitPoints;
    }


    public void TakeDamage(float damage)
    {
        hitPoints -= damage;
        if (hitPoints < 0f)
        {
            if (isPlayer) FindObjectOfType<Game>().GameOver();
            else Destroy(gameObject);
        }
    }
}


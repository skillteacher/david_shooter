using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
public class EnemyData : MonoBehaviour
{
    public Transform target;
    public Health targetHealth;
    public Animator animator;
    public NavMeshAgent agent;
    public float distanceToTarget;
    private void Awake()
    {
        targetHealth = target.GetComponent<Health>();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }
    private void Update()
    {
         distanceToTarget = Vector3.Distance(target.position, transform.position);
    }
}